import React from 'react'

import facebook from '../assets/facebook-official.png'
import twitter from '../assets/twitter.png'

const Footer = () => {
    return (
        <>
            <div class="flex flex-col gap-2 justify-center px-4 py-6 bg-gray-800 dark:bg-gray-00 md:items-center mt-20">
                <div>
                    <span class="text-sm text-white dark:text-gray-300 sm:text-center"> Copyright &copy; 2016. PT Company </span>
                </div>
                <div class="flex mt-4 space-x-5 sm:justify-center md:mt-0">
                    <a href="#" class="text-gray-400 hover:text-gray-900 dark:hover:text-white w-[20px]">
                        <img src={facebook} alt=""/>
                    </a>
                    <a href="#" class="text-gray-400 hover:text-gray-900 dark:hover:text-white w-[20px]">
                        <img src={twitter} alt="" />
                    </a>
                </div>
            </div>
        </>
    )
}

export default Footer