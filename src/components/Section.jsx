import React from 'react'
import './Section.css'
import bg from '../assets/bg.jpg'
import aboutbg from '../assets/about-bg.jpg'

// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

// import required modules
import { Pagination, Navigation } from 'swiper/modules';

const Section = () => {
    return (
        <>
            <Swiper
                slidesPerView={1}
                spaceBetween={30}
                loop={true}
                pagination={{
                    clickable: true,
                }}
                navigation={true}
                modules={[Pagination, Navigation]}
                className='mySwiper'
            >
                <SwiperSlide>
                    <img src={bg} alt='' />
                    <h3 className='absolute px-4 text-lg text-white bottom-20 pl-20 font-semibold'>THIS IS A PLACE WHERE TECHNOLOGY & CREATIVITY FUSED INTO DIGITAL CHEMISTRY</h3>
                </SwiperSlide>
                <SwiperSlide>
                    <img src={aboutbg} alt='' />
                    <h3 className='absolute px-4 text-lg text-white bottom-20 pl-20 font-semibold'>WE DON'T HAVE THE BEST BUT WE HAVE THE GREATEST TEAM</h3>
                </SwiperSlide>
            </Swiper>
        </>
    )
    }

export default Section