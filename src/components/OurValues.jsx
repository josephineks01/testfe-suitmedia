import React from 'react'

import balancescale from '../assets/balance-scale.png'
import bank from '../assets/bank.png'
import lightbulb from '../assets/lightbulb-o.png'

const OurValues = () => {
    return (
        <>
            <div className='text-center mt-20'>
                <h1 className='text-black text-2xl font-bold'>OUR VALUES</h1>
            </div>

            <div className='flex overflow-hidden items-center justify-around mt-10 px-20'>
                <div className='text-center'>
                    <a href='#' className='block max-w-sm p-6 bg-red-600 border border-gray-200 rounded-lg shadow hover:bg-red-600'>
                        <img className='mx-auto' src={lightbulb} alt='' />
                        <h5 className='mb-2 text-2xl font-bold tracking-tight text-white mt-2'>INNOVATIVE</h5>
                        <p className='font-normal text-white'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non blandit justo, eu porttitor sapien. Suspendisse sed dapibus enim.</p>
                    </a>
                </div>

                <div className='text-center'>
                    <a href='#' className='block max-w-sm p-6 bg-green-600 border border-gray-200 rounded-lg shadow hover:bg-green-600'>
                        <img className='mx-auto' src={bank} alt='' />
                        <h5 className='mb-2 text-2xl font-bold tracking-tight text-white mt-2'>LOYALTI</h5>
                        <p className='font-normal text-white'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed ante metus. Pellentesque nec maximus mauris, eget consequat magna.</p>
                    </a>
                </div>

                <div className='text-center'>
                    <a href='#' className='block max-w-sm p-6 bg-blue-600 border border-gray-200 rounded-lg shadow hover:bg-blue-600'>
                        <img className='mx-auto' src={balancescale} alt='' />
                        <h5 className='mb-2 text-2xl font-bold tracking-tight text-white mt-2'>RESPECT</h5>
                        <p className='font-normal text-white'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ullamcorper gravida leo nec congue. Donec lacinia sagittis condimentum. Nulla facilisi.</p>
                    </a>
                </div>
            </div>
        </>
        
    )
}

export default OurValues