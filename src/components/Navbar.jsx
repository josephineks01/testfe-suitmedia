import React from 'react'
import './Navbar.css'

const Navbar = () => {
    return (
        <div className='navbar flex overflow-hidden bg-white items-center justify-around'>
            <h1 className='text-black text-2xl font-bold'>Company</h1>
            <div className='flex items-center'>
                <div className='dropdown overflow-hidden'>
                    <div className='dropbtn border-none outline-none text-black p-4 bg-white'>
                        ABOUT 
                        <i className='fa fa-caret-down'></i>
                    </div>
                    <div className='dropdown-content z-50'>
                        <a href='#'>HISTORY</a>
                        <a href='#'>VISSION MISSION</a>
                    </div>
                </div>
                <div className='flex items-center'>
                    <a href='#news' className='p-4'>OUR WORK</a>
                    <a href='#news' className='p-4'>OUR TEAM</a>
                    <a href='#news' className='p-4'>CONTACT</a>
                </div>
            </div>
            
        </div>
    )
}

export default Navbar