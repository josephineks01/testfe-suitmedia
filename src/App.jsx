import React from "react"
import Navbar from "./components/Navbar"
import Section from "./components/Section"
import OurValues from "./components/OurValues"
import ContactUs from "./components/ContactUs"
import Footer from "./components/Footer"

export default function App() {
  return (
    <>
      <Navbar />
      <Section />
      <OurValues/>
      <ContactUs/>
      <Footer/>
    </>
  )
}